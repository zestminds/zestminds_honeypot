#!/bin/sh

export PYTHONPATH="$PWD/app"

twistd --logfile=/var/log/nu-honeypot.log --pidfile=/var/run/nu-honeypot.pid -y $PYTHONPATH/honeypot.tac
