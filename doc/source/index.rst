Welcome To NU-Honeypot suite documentation!
============================================

Use this project only if you know what you are doing.

Dont forget to edit the honeypot.cfg file before starting.

Proceed the documentation for more details.

Overview on How to Run this Script
====================================
1. First clone the project through git from repository.

2. Run command ". ./install.sh". This command will install all the dependencies and make a virtualenvironment itself.

3. After complete this command process run '. run.sh' command will start the telnet and pop3 protocols.


Documentation for the Code!
================================  


.. toctree::
   :maxdepth: 2
   :caption: Contents:

Queries Module
===================
.. automodule:: utils.queries
   :members:
   
Constant Module
===================
.. automodule:: utils.constants
   :members:
   
   
Reporter Module
===================
.. automodule:: reporter
.. autoclass:: ReporterFactory
   :members:
  
Configuration Module
=======================
.. automodule:: conf
.. autoclass:: ConfigFactory
   :members:

Telnet Module
===================   
.. automodule:: telnet
.. autoclass:: TelnetPotProtocol
   :members:
   
POP3 Module
===================
.. automodule:: pop3
.. autoclass:: FakePop3Session
   :members:
   

   


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

