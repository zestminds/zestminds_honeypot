# Honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import configparser
import logging
import os
import sys
from time import strftime

from twisted.python import log
from twisted.python.logfile import LogFile

import reporter
from utils.queries import TABLE
from pymongo import MongoClient


class ConfigFactory:

    def __init__(self):
        """Initialize the mysql database 
        telnet server and the pop3 settings
        argument -- [ self ]
        return: object
        """

        config = configparser.ConfigParser()
        config.read('honeypot.cfg')
        dbname = config.get('MongoDb', 'dbname')
        dbuser = config.get('MongoDb', 'dbuser')
        dbpass = config.get('MongoDb', 'dbpass')
        dbhost = config.get('MongoDb', 'dbhost')
        dbport = config.get('MongoDb', 'dbport')
        self.dbenabled = config.getboolean('MongoDb', 'enabled')
        self.telnetEnabled = config.get('Telnet', 'enabled')
        self.pop3Enabled = config.get('POP3', 'enabled')
        self.redisEnabled = config.get('Redis', 'enabled')
        self.ftpEnabled = config.get('Smtp', 'enabled')
        self.smtpEnabled = config.get('Ftp', 'enabled')
        self.tftpEnabled = config.get('Tftp', 'enabled')
        self.httpEnabled = config.get('Http', 'enabled')
        self.dnsEnabled = config.get('Dns', 'enabled')
        self.nucleonEnabled = config.get('Reporting', 'nucleon')
        self.p0fEnabled = config.get('p0f', 'enabled')
        # ~ print00se0000lf00.p0fEnabled)
        self.pop3logfile = config.get('POP3', 'logfile')
        self.redislogfile = config.get('Redis', 'logfile')
        self.ftplogfile = config.get('Ftp', 'logfile')
        self.pop3ServerPort = config.getint('POP3', 'service_port')
        self.telnetServerPort = config.getint('Telnet', 'service_port')
        self.redisServerPort = config.getint('Redis', 'service_port')
        self.ftpServerPort = config.getint('Ftp', 'service_port')
        self.smtpServerPort = config.getint('Smtp', 'service_port')
        self.tftpServerPort = config.getint('Tftp', 'service_port')
        self.httpServerPort = config.getint('Http', 'service_port')
        self.dnsServerPort = config.getint('Dns', 'service_port')
        self.p0fsocket = config.get('p0f', 'socket')
        # ~ print(self.p0fsocket)
        self.nureports = config.get('Reporting', 'nucleon')
        self.dbpool = False

        self.logger = logging.getLogger('Telnet')
        self.logger.addHandler(logging.StreamHandler(sys.stderr))
        # logger.addHandler(logging.FileHandle(logfile))
        for h in self.logger.handlers:
            h.setFormatter(logging.Formatter(
                fmt='%(asctime)s|[%(name)s.%(levelname)s %(lineno)d]:| %(message)s'))

        self.logger.setLevel(logging.DEBUG)

        if self.dbenabled:
            uri = "mongodb://"+dbuser+":"+dbpass+"@"+dbhost+":"+dbport
            db = MongoClient(uri)
            self.db = db[dbname]
            
            # ~ self.db = MySQLdb.connect(host=dbhost,
                                      # ~ user=dbuser,
                                      # ~ passwd=dbpass,
                                      # ~ )
            # ~ self.cur = self.db.cursor()
            # ~ print(self.cur)
            # ~ self.create_table()

    # def create_table(self):
        # """Creates the database and the tables in mysql
        # argument -- [ self ]
        # return: object
        # """

        # self.cur.execute(TABLE['CREATE_DB'])
        # self.cur.execute(TABLE['USE_DB'])
        # self.cur.execute(TABLE['SESSION_TABLE'])
        # self.cur.execute(TABLE['USER_TABLE'])
        # self.cur.execute(TABLE['CMDS_TABLE'])

    def log_to_db(self,dbTable,record):
        """Updates the tables in mysql
        argument -- [ self,sql ]
        return: object
        """
        if self.db:
            if(dbTable == 'telnetUsers' or dbTable == 'pop3Users'):
                usersCollection = self.db['users']
                self.db.users.insert(record) 
            if(dbTable == 'telnetSessions' or dbTable == 'pop3Sessions' or dbTable=='smtpSessions'):
                sessionCollection = self.db['sessions']
                self.db.sessions.insert(record) 
            if(dbTable == 'telnetCmd' or dbTable == 'pop3Cmd' or dbTable == 'smtpCmd'):
                cmdCollection = self.db['cmds']
                self.db.cmds.insert(record)
            if(dbTable == 'ftpPots'):
                ftpCollection = self.db['ftpPot']
                self.db.ftpPot.insert(record)	
            print("report is %s" % record)

        if self.nureports:
            r = reporter.ReporterFactory()
            # ~ print(r)
            # ~ return
            r.send_report(record)
            
