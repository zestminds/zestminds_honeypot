"""Client/manager side object for querying Agent via SNMPProtocol"""
import pysnmpproto
if pysnmpproto.pysnmpversion == 4:
	from v4.agentproxy import *
else:
	from v3.agentproxy import *
