from twisted.web import server, resource
from twisted.protocols import basic
from twisted.internet import reactor, defer,protocol
from pprint import pprint
import base64


class DummyServer(resource.Resource):
    isLeaf = True
    
    # def __init__(self):
        # self.lines = []

    # def lineReceived(self, line):
        # self.lines.append(line)
        # if not line:
            # self.sendResponse()

    def sendResponse(self):   
        self.sendLine(b"HTTP/1.1 200 OK")
        self.sendLine(b"")
        responseBody = "You said:\r\n\r\n" + "\r\n".join(str(self.lines))
        self.transport.write(responseBody.encode('utf-8'))
        self.transport.loseConnection()

    def returnContent(self, deferred, request, msg):
        print ("Finishing request to '%s'" % request.uri)
        request.write(msg)
        request.finish()

    def cancelAnswer(self, err, request, delayedTask):
        print ("Cancelling request to '%s': %s" % \
            (request.uri, err.getErrorMessage()))
        delayedTask.cancel()

    def render_GET(self, request):
        print ("Received request for '%s'" % request.uri)
        if request.uri == b'/delayed':
            print ("Delaying answer for '/delayed'")
            d = defer.Deferred()
            delayedTask = reactor.callLater(60, self.returnContent, d,
                                            request, b"Hello, delayed world!")
            request.notifyFinish().addErrback(self.cancelAnswer,
                                              request, delayedTask)
            return server.NOT_DONE_YET
        elif request.uri == b'/protected':
            auth = request.getHeader(b'Authorization')
            if auth and auth.split(b' ')[0] == b'Basic':
                decodeddata = base64.decodebytes(auth.split(b' ')[1])
                if decodeddata.split(b':') == [b'username', b'password']:
                    return b"Authorized!"

            request.setResponseCode(401)
            request.setHeader('WWW-Authenticate', 'Basic realm="realmname"')
            return b"Authorization required."
        else:
            print ("Immediate answering request for '/'")
            # self.sendLine(b"HTTP/1.1 200 OK")
            # self.sendLine(b"")
            # responseBody = "You said:\r\n\r\n" + "\r\n".join(str(self.lines))
            # self.transport.write(responseBody.encode('utf-8'))
            # self.transport.loseConnection()
            return b"Hello, world! %s" % request.uri
            

# s = server.Site(DummyServer())
# reactor.listenTCP(8000, s)
# reactor.run()