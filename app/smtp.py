# SMTP honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA	 02110-1301, USA.

# import ConfigParser
import logging
import os
import os.path
import sys
import time
import unicodedata
import uuid

from twisted.enterprise import adbapi
from twisted.internet import reactor,protocol
from twisted.internet.protocol import Factory
from twisted.protocols.basic import LineReceiver
from twisted.mail.smtp import SMTP
from twisted.python import log

import conf


class SmtpPotProtocol(LineReceiver,SMTP):
	COMMAND = 0
	DATA = 1
	def __init__(self):
		"""Initialise the session, state,name and configuration
		argument -- [ self,data ]
		return: object
		"""
		self.name = None
		self.msg_from = None
		self.msg_to = []
		self.data = ''
		self.delimiter = b'\n'
		self.session = str(uuid.uuid1())
		self.state = self.COMMAND
		self.hostname = ''
		self.conf = conf.ConfigFactory()
		
		
	def connectionMade(self):
		"""Saves the session to mysql table
		argument -- [ self ]
		return: object
		"""
		self.transport.write(
			("+OK SMTP (version 2.2) at %s starting.\r\n" % self.transport.getPeer().host).encode('utf8'))
		sessionRecord = {
				"session": ""+self.session+"",
				"ip" : ""+self.transport.getPeer().host+"",
				"protocol" : "smtp",
				"port" : ""+str(self.transport.getHost().port)+"",
				}
		self.conf.log_to_db("smtpSessions",sessionRecord)


	def lineReceived(self, line):
		"""Saves the session commands to mongodb table
		argument -- [ self,line ]
		return: object
		"""
		if self.state == self.COMMAND:
			line = line.replace(b"\r", b"")
			i = line.find(b' ')
			if i < 0:
				command = line.upper()
				arg = None
			else:
				command = line[:i].upper()
				arg = line[i+1:].strip()
			# command = line.upper()	
			cmdsRecord = {
					"session": ""+self.session+"",
					"cmd" : ""+line.decode()+"",
					}
			self.conf.log_to_db("smtpCmd",cmdsRecord)
			
			if command.startswith(b'QUIT'):
				self.transport.write(("221 %s ESMTP server closing connection\r\n" % self.transport.getPeer().host).encode())
				self.transport.loseConnection()
			else:
				method = getattr(self, 'smtp_' + command.decode(), None)
				print(method)
				if not method:
					self.transport.write(('502 Error: command "%s" not implemented\r\n' % command.decode()).encode())
					return
				method(line)
		else:
			if self.state != self.DATA:
				self.transport.write('451 Internal confusion')
				return
			data = []
			for text in line.split(b'\r\n'):
				if text and text[0] == b'.':
					data.append(text[1:])
				else:
					data.append(text)
			msg_body = b'\n'.join(data)
			if(msg_body == b'.\r'):
				msg_body = msg_body.decode()+"\r\n"
				self.data = msg_body
				self.state = self.COMMAND
				self.transport.write(b'250 Ok\r\n')
				
	def smtp_HELO(self, command):
		arg = command[len(b'HELO'):].strip()
		if not arg:
			self.transport.write(b'501 Syntax: HELO hostname\r\n')
			return
		else:
			self.transport.write(('250 Hello {} at your service\r\n'.format(self.transport.getPeer().host)).encode())	

	def smtp_EHLO(self, command):
		arg = command[len(b'EHLO'):].strip()
		if not arg:
			self.transport.write(b'501 Syntax: EHLO hostname\r\n')
			return
		else:
			self.transport.write(('250 Hello {} at your service\r\n250-8BITMIME\r\n250-PIPELINING\r\n250 STARTTLS\r\n'.format(self.transport.getPeer().host)).encode())
			
	def smtp_MAIL(self, command):
		msg_from = command[len(b'MAIL FROM:'):].strip()
		if not msg_from:
			self.transport.write(b'501 Syntax: MAIL FROM: address\r\n')
			return
		else:
			self.msg_from = msg_from
			self.transport.write(("250 Sender: {} Ok\r\n".format(msg_from)).encode())
	
	def smtp_RCPT(self, command):
		if not self.msg_from:
			self.transport.write(b'503 Error: Need MAIL FROM Command\r\n')
			return
		else:
			msg_to = command[len(b'RCPT TO:'):].strip()
			if not msg_to:
				self.transport.write(b'501 Syntax: RCPT TO: address\r\n')
				return
			else:
				self.msg_to = msg_to
				self.transport.write(("250 Sender: {} Ok\r\n".format(msg_to)).encode())
	
	def smtp_RSET(self, command):
		self.msg_from = None
		self.msg_to = []
		self.data = ''
		self.state = self.COMMAND
		self.transport.write(b"250 Reset Ok\r\n")
	
	def smtp_DATA(self, command):
		if not self.msg_to:
			self.transport.write(b'503 Error: Need RCPT TO Command\r\n')
			return
		if not command:
			self.transport.write(b'501 Syntax: DATA')
			return
		self.state = self.DATA
		self.transport.write(b'354 Enter mail, end with "." on a line by itself\r\n')
	