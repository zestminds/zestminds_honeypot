# Honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

# import ConfigParser
import getpass
import logging
import os
import os.path
import re
import sys
import time
import unicodedata
import uuid
from urllib.request import urlopen
from twisted.conch.telnet import StatefulTelnetProtocol
from twisted.enterprise import adbapi
from twisted.internet import reactor , protocol ,endpoints
# ~ from twisted.internet import 
from twisted.internet.protocol import ServerFactory
from twisted.protocols.basic import LineReceiver
from twisted.python import log

import conf
import pof
# ~ from geoip import geolite2
from geolite2 import geolite2


class TelnetPotProtocol(StatefulTelnetProtocol):

    state = 'User'
    def __init__(self):
        """Initialise the ConfigFactory
        and p0fFactory
        argument -- [ self ]
        return: object
        """
        self.delimiter = b'\n'
        self.session = str(uuid.uuid1())
        self.firstLogin = True
        state = 'User'
        self.conf = conf.ConfigFactory()
        self.p0f = pof.p0fFactory()

    def telnet_Password(self, line):
        """Saves the user details in mysql table
        argument -- [ self,line ]
        return: object
        """
        #here get the username and password for save the user data like username,password and session.
        username, password = self.username, line
        password = password.decode().rstrip('\r') #decode the bytes into string here for right side strip for raw string
        #self.logger.debug(u'%s|lp|%s|%r,%r' % (self.session, self.transport.getHost().host,username,password))
        
        userRecord = {
                "session": ''+self.session+'',
                "username" : ""+username+"",
                "password" : ""+password+""
                }
        self.conf.log_to_db("telnetUsers",userRecord)
        
        # use the passsword 12345 will go to the print_banner() function and shows the server login. but if password is not 12345 then authentication failed message again and again. 
        if password.startswith('12345'):
            self.print_banner()
            self.state = "Command"
            self.print_banner()
            #this will return commands means got to telnet_Command() function for use the commands like ls , free ,etc.
            return 'Command'
        else:
            del self.username
            self.transport.write(b"\nAuthentication failed\n")  #Change into bytes
            self.transport.write(b"Username: ")  #Change into bytes
            self.state = "User"
            return 'User'

    def connectionMade(self):
        """Saves the session details details in mysql table
        argument -- [ self ]
        return: object
        """
        self.transport.write(b"Welcome To Babe web.\r\n") #Change into bytes
        self.transport.write(b"Username: ")#Change into bytes
        p0fResult = self.p0f.get_ip(self.transport.getPeer().host)

# Changes for getting country and locations        

        reader = geolite2.reader()
        if self.transport.getPeer().host == '192.168.1.18':
            my_ip = urlopen('http://ip.42.pl/raw').read().decode()
            match = reader.get(my_ip)
        else:
            match = reader.get(self.transport.getPeer().host)
        cntry = str(match['country']['names']['en']) #make str and get name from dict
        loc = match['location'] #get location from dict
        lat1 = str(loc['latitude']) #Change to str and get by name instead of array values
        lon1 = str(loc['longitude']) #Change to str and get by name instead of array values
        try:
            if not p0fResult["language"]:
                p0fResult["language"] = ""
            if not p0fResult["http_name"]:
                p0fResult["http_name"] = ""
            if not p0fResult["os_name"]:
                p0fResult["os_name"] = ""
            if not p0fResult["uptime_sec"]:
                p0fResult["uptime_sec"] = "" 
            if not p0fResult["os_flavor"]:
                p0fResult["os_flavor"] = ""
            if not p0fResult["http_flavor"]:
                p0fResult["http_flavor"] = ""
        except:
            # self.transport.write("err")
            print('')
        #Changes in insert query for inserting data properly
        sessionRecord = {
                "session": ""+self.session+"",
                "ip" : ""+self.transport.getPeer().host+"",
                "protocol" : "telnet",
                "p0f_os" : ""+str(p0fResult["os_name"].decode())+"",
                "p0f_uptime" : ""+str(p0fResult["uptime_sec"])+"",
                "p0f_os_flavor" : ""+str(p0fResult["os_flavor"].decode())+"",
                "p0f_http_flavor" : ""+str(p0fResult["http_flavor"].decode())+"",
                "p0f_language" : ""+str(p0fResult["language"].decode())+"",
                "p0f_http_name" : ""+str(p0fResult["http_name"].decode())+"",
                "port" : ""+str(self.transport.getHost().port)+"",
                "country" : ""+cntry+"",
                "lat" : ""+lat1+"",
                "lon" : ""+lon1+"",
                }
        self.conf.log_to_db("telnetSessions",sessionRecord)
        
        # ~ self.conf.log_to_db("INSERT INTO `sessions` (session,ip,protocol,p0f_os,p0f_uptime,p0f_os_flavor,p0f_http_flavor,p0f_language,p0f_http_name,port,country,lat,lon)VALUES('"+self.session+"','"+self.transport.getPeer().host+"','telnet','"+str(p0fResult["os_name"].decode())+"','"+str(p0fResult["uptime_sec"])+"','"+str(p0fResult["os_flavor"].decode())+"','"+str(p0fResult["http_flavor"].decode())+"','"+str(p0fResult["language"].decode())+"','"+str(p0fResult["http_name"].decode())+"','"+str(self.transport.getHost().port)+"','"+cntry+"','"+lat1+"','"+lon1+"')")
    
    def connectionLost(self, reason):
        # ~ self.factory.connectionPool.remove(self.session)
        logging.info(b'Connection lost. Session ended')
    
    def telnet_User(self, line):
        # After save the session in database from "connectionMade" function trying to send the report through reporter.py and then come in this function.
        if self.firstLogin == True:
            self.username = line.replace(b"'", b'') #changes to bytes
            self.username = self.username.replace(b" ", b"")  #changes to bytes
            self.username = re.sub(r'"', '', self.username.decode())  #decode the username from bytes to string for re.sub()
            self.firstLogin = False
        else:
            # if self.firstLogin is false then it comes to there
            self.username = re.sub(r'"', '', line.decode()) #decode the username from bytes to string for re.sub()
        self.username = self.username.rstrip('\r')
        self.transport.write(b'Password: ')
        #this function is return the password in bytes to telnet_Password()
        return 'Password'

    def telnet_logged_in(self, line):
        self.transport.write(b"root@prod03:~# ")

    def enableRemote(self, option):
        return False

    def disableRemote(self, option):
        pass

    def enableLocal(self, option):
        return False

    def disableLocal(self, option):
        pass

    def print_banner(self):
        #change all string into bytes for write the data like root@prod03:~#.
        self.transport.write(
            b"The programs included with the Debian GNU/Linux system are free software;\n")
        self.transport.write(
            b"the exact distribution terms for each program are described in the\n")
        self.transport.write(
            b"individual files in /usr/share/doc/*/copyright.\n\n")
        self.transport.write(
            b"Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent\n")
        self.transport.write(b"permitted by applicable law.\n\n")
        self.transport.write(b"root@prod03:~# ")
    
    
    def telnet_Command(self, cmd):
        """Saves the telnet commands in mysql table
        argument -- [ self,cmd ]
        return: object
        """
        # type anything from command line after login will be cmd and decode the cmd into string from bytes because string is concetinate only strings.
        cmdsRecord = {
                "session": ""+self.session+"",
                "cmd" : ""+cmd.decode()+"",
                }
        self.conf.log_to_db("telnetCmd",cmdsRecord)
        # ~ self.conf.log_to_db(
            # ~ "INSERT INTO `cmds` (session,cmd) VALUES('"+self.session+"','"+cmd.decode()+"')")
        #logger.debug(u'%s|cmd|%s|%r' % (self.session, self.transport.getHost().host,cmd))
        #change strings into byte here also.
        if (cmd in [b'quit', b'exit']):
            #change the quit and exit will be in bytes not in raw string.
            #log & close
            self.transport.loseConnection()
        elif (cmd.startswith(b'ls')):
            file = open("cmd/ls.txt")
            self.transport.write(file.read().encode())
        elif (cmd.startswith(b'free')):
            file = open("cmd/free.txt")
            self.transport.write(file.read().encode())
        elif (cmd.startswith(b'id')):
            file = open("cmd/id.txt")
            self.transport.write(file.read().encode())
        elif (cmd.startswith(b'uname')):
            file = open("cmd/uname.txt")
            self.transport.write(file.read().encode())
        elif (cmd.startswith(b'wget')):
            file = open("cmd/wget.txt")
            self.transport.write(file.read().encode())
        elif (cmd.startswith(b'echo')):
            a = cmd.split(b" ")
            self.transport.write(a[1])
            self.transport.write(b"\n")
        else:
            a = cmd.rstrip()
            self.transport.write(b"%r: command not found\n" % a)
        self.transport.write(b"root@prod03:~# ")

