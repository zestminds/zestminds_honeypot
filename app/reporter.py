# Honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
import os
import sys
from urllib.request import urlopen

import requests
from twisted.python.logfile import LogFile

from utils.constants import REPORT_FACTORY



class ReporterFactory:
    def send_report(self, data):
        """Post the response
        argument -- [ self,data ]
        return: object
        """
        host_ip = urlopen(REPORT_FACTORY['HOST_IP']).read()
        response = requests.request("POST",
            REPORT_FACTORY['REPORT_URL'],
            headers={"X-Mashape-Key": REPORT_FACTORY['X_MASHAPE_KEY'],
                     "Content-Type": 'application/x-www-form-urlencoded'},
            params={"data": str(data), "ip": host_ip}
        )
        # ~ #here report_url is getting 505 server timeout error thats why in exception pass directly
        try:
            json_response = response.json()
        except Exception as e:
            pass
            # ~ print(e,'i am here')

# ~ ReporterFactory().send_report("fjfgf")
