total 136
drwx------  8 root root  4096 Oct 23 03:20 .
drwxr-xr-x 24 root root  4096 Oct 20 06:30 ..
drwx------  2 root root  4096 Aug 27 03:15 .aptitude
-rw-------  1 root root 56967 Oct 22 14:39 .bash_history
-rw-r--r--  1 root root  3106 Feb 19  2014 .bashrc
drwx------  2 root root  4096 Jun 23 02:59 .cache
-rw-r--r--  1 root root     0 Jun 23 03:00 .cloud-locale-test.skip
drwxr-xr-x  3 root root  4096 Oct 16 14:41 .composer
-rw-r--r--  1 root root    50 Jun 30 05:46 .gitconfig
-rw-------  1 root root  4665 Oct 23 02:31 .mysql_history
-rw-r--r--  1 root root   140 Feb 19  2014 .profile
-rw-r--r--  1 root root   297 Oct  5 15:27 .rediscli_history
drwx------  2 root root  4096 Sep 30 06:00 .ssh
drwxr-xr-x  3 root root  4096 Oct 16 14:49 .subversion
drwxr-xr-x  2 root root  4096 Oct 17 14:02 .vim
-rw-------  1 root root 17010 Oct 23 03:20 .viminfo
